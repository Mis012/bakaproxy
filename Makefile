GCC := /home/Mis012/Android/ndk-standalone/bin/arm-linux-androideabi-gcc

default: android
	
android: main.c
	${GCC} main.c xml.c ./lib/gio/modules/static/libgiognutls.a lib/libsoup-2.4.a lib/libgnutls.a lib/libhogweed.a lib/libtasn1.a lib/libgmp.a lib/libnettle.a lib/libxml2.a lib/libgio-2.0.a lib/libgobject-2.0.a lib/libgmodule-2.0.a lib/libglib-2.0.a lib/libz.a lib/libiconv.a lib/libintl.a lib/libffi.a -I./include/ -I./include/glib-2.0/ -I./lib/glib-2.0/include/ -I./include/libsoup-2.4 -I./include/libxml2 -lm -ldl -o BakaProxy -pie
	
gnu_linux: main.c
	gcc main.c xml.c -lsoup-2.4 -lgio-2.0 -lgobject-2.0 -lglib-2.0 -pthread -I/usr/include/libsoup-2.4 -I/usr/include/glib-2.0 -I/usr/lib64/glib-2.0/include -lz -llzma -lm -ldl -o BakaProxy -g
