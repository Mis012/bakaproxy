/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include <libsoup/soup.h>
#include <glib/gstdio.h>

#include "xml.h"

#define BAKALARI_URL "https://bakalari.gymso.cz/"

extern void g_io_module_gnutls_load_static (void);

SoupSession *session;

void debug_printf(const char *format, ...) {
	#ifdef DEBUG
	va_list args;
	va_start(args, format);
	vprintf(format, args);
	va_end(args);
	#endif
}


static void
server_callback (SoupServer *server, SoupMessage *msg,
		 const char *path, GHashTable *query,
		 SoupClientContext *context, gpointer data)
{
	SoupMessageHeadersIter iter;
	char *full_request = soup_uri_to_string(soup_message_get_uri(msg), TRUE);
	char *full_url = malloc(strlen(BAKALARI_URL)*sizeof(char) + strlen(full_request)*sizeof(char) + 1*sizeof(char));
	*(full_url) = 0;
	strcpy(full_url, BAKALARI_URL);
	strcat(full_url, full_request);
	
	SoupMessage *send_msg = soup_message_new (msg->method, full_url);
	if(send_msg == NULL) {
		printf("send_msg is NULL.\n");
	}
	soup_message_set_request (send_msg, "application/json", SOUP_MEMORY_COPY, msg->request_body->data, msg->request_body->length);
	
	soup_session_send_message (session, send_msg);

	debug_printf ("url: >>%s<<\nmethod: >>%s<<\nrequest:>>%s<<\n\n\nresponse:>>%s<<\n", full_url, msg->method, msg->request_body->data,send_msg->response_body->data);

	if(send_msg->response_body->data == NULL) {
		printf("libsoup - error sending: %s\n",send_msg->reason_phrase);
	}

	char *edited_response = NULL;
	int edited_response_size;

	if(strcasestr(send_msg->response_body->data, "<rozvrh>")) {
		//edited_response = (char*)str_replace((char *)send_msg->response_body->data,"<zkrmist>7. A</zkrmist>", "<zkrmist>A 309</zkrmist>");
		xml_replace((char *)send_msg->response_body->data, send_msg->response_body->length, (xmlChar **)&edited_response, &edited_response_size);
	}
	if (edited_response == NULL) {
		edited_response = (char*)send_msg->response_body->data;
		edited_response_size = send_msg->response_body->length;
	}
	
	debug_printf("after editing:\n>>>\n%s\n<<<\n", edited_response);

	soup_message_set_response (msg, "text/xml", SOUP_MEMORY_COPY, edited_response, strlen(edited_response));

	if(edited_response != send_msg->response_body->data) { //this is legal ;) yay! Feel free to question if it's a good idea though.
		free(edited_response);
	}
	g_object_unref(send_msg);

	soup_message_set_status (msg, SOUP_STATUS_OK);

	debug_printf ("  -> %d %s\n\n", msg->status_code, msg->reason_phrase);
}

static int port = 30000;

int main (int argc, char **argv)
{
	GMainLoop *loop;
	SoupServer *server;
	GSList *uris, *u;
	char *str;
	GTlsCertificate *cert;
	GError *error = NULL;
	
	g_io_module_gnutls_load_static ();
	
	server = soup_server_new (SOUP_SERVER_SERVER_HEADER, "simple-httpd ", NULL);
	soup_server_listen_local (server, port, 0, &error);

	soup_server_add_handler (server, NULL, server_callback, NULL, NULL);

	session = soup_session_new_with_options (SOUP_SESSION_SSL_STRICT, FALSE, NULL);

	uris = soup_server_get_uris (server);
	for (u = uris; u; u = u->next) {
		str = soup_uri_to_string (u->data, FALSE);
		printf ("Listening on %s\n", str);
		g_free (str);
		soup_uri_free (u->data);
	}
	g_slist_free (uris);

	printf ("\nWaiting for requests...\n");

	loop = g_main_loop_new (NULL, TRUE);
	g_main_loop_run (loop);

	return 0;
}

