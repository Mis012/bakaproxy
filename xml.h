#ifndef BAKAPROXY_XML_H
#define BAKAPROXY_XML_H

#include <libxml/tree.h>

int xml_replace(char *original, int original_size, xmlChar **modified, int *modified_size);
#endif
