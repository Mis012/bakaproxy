GCC=arm-linux-androideabi-gcc
gstreamer_arch="armv7"

gstreamer_ver="1.12.3"
gstreamer_filename="gstreamer-1.0-android-universal-${gstreamer_ver}.tar.bz2"
gstreamer_url="http://gstreamer.freedesktop.org/data/pkg/android/${gstreamer_ver}/${gstreamer_filename}"

mkdir tmp
cd tmp

echo "downloading gstreamer's prebuilt libraries ..."
wget ${gstreamer_url}
echo "extracting ..."
tar -xjf ${gstreamer_filename}

echo "extracted! symlinking lib and include ..."

ln -s ${gstreamer_arch}/lib ../
ln -s ${gstreamer_arch}/include ../

echo "./lib and ./include should now exist"
