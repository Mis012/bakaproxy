#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

static void update_xpath_nodes(xmlNodeSetPtr nodes, const xmlChar * value);


int xml_replace(char *original, int original_size, xmlChar **modified, int *modified_size) {
    
    *modified = NULL;
    
    /* Init libxml */
    xmlInitParser();
    LIBXML_TEST_VERSION

    
    FILE *class_mapping_file;
  
	class_mapping_file = fopen("class_mapping.txt","r");
	if(class_mapping_file == NULL) {
		printf("class_mapping.txt couldn't be opened");
		return -1;
	}
	//count lines vv
	int lines = 0;
	int ch;
	while((ch = fgetc(class_mapping_file)) != EOF)
	{
		if(ch == '\n')
		{
			lines++;
		}
	}
	fseek(class_mapping_file, 0, SEEK_SET);
	//count lines ^^
	
	xmlDocPtr doc;
    xmlXPathContextPtr xpathCtx; 
    xmlXPathObjectPtr xpathObj; 
	
	/* Load XML document */
    doc = xmlParseMemory(original, original_size);
    if (doc == NULL) {
	printf("Error: unable to parse this: ```\n%s\n```\n", original);
	return(-1);
    }

    /* Create xpath evaluation context */
    xpathCtx = xmlXPathNewContext(doc);
    if(xpathCtx == NULL) {
        printf("Error: unable to create new XPath context\n");
        xmlFreeDoc(doc); 
        return(-1);
    }
	
	char buffer[200];
	int i;
	
	for(i=0; i<lines; i++) {
		if(fgets(buffer, sizeof(buffer), class_mapping_file) == NULL) {
			break;
		}
		
		char *split = strchr(buffer, ';');
		if(split == NULL) {
			printf("split is NULL. Malformed mapping file? crash in 3..2..1\n");
		}
		*(split) = '\0';
		char *xpathExpr = buffer;
		char *value = (split + 1);
		*(strrchr(value, '\n')) = '\0';
		
		/* Evaluate xpath expression */
		xpathObj = xmlXPathEvalExpression(xpathExpr, xpathCtx);
		if(xpathObj == NULL) {
		    fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", xpathExpr);
		    xmlXPathFreeContext(xpathCtx); 
		    xmlFreeDoc(doc); 
		    return(-1);
		}

		/* update selected nodes */
		update_xpath_nodes(xpathObj->nodesetval, value);

		
		/* Cleanup of XPath data */
		xmlXPathFreeObject(xpathObj);
	}
    
    xmlXPathFreeContext(xpathCtx); 

    /* dump the resulting document */
    xmlDocDumpMemory(doc, modified, modified_size);


    /* free the document */
    xmlFreeDoc(doc); 
    
    

    /* Shutdown libxml */
    xmlCleanupParser();
    
    /*
     * this is to debug memory for regression tests
     */
    xmlMemoryDump();
    return *modified_size;
}

/**
 * update_xpath_nodes:
 * @nodes:		the nodes set.
 * @value:		the new value for the node(s)
 *
 * Prints the @nodes content to @output.
 */
static void
update_xpath_nodes(xmlNodeSetPtr nodes, const xmlChar* value) {
    int size;
    int i;
    
    assert(value);
    size = (nodes) ? nodes->nodeNr : 0;
    
    /*
     * NOTE: the nodes are processed in reverse order, i.e. reverse document
     *       order because xmlNodeSetContent can actually free up descendant
     *       of the node and such nodes may have been selected too ! Handling
     *       in reverse order ensure that descendant are accessed first, before
     *       they get removed. Mixing XPath and modifications on a tree must be
     *       done carefully !
     */
    for(i = size - 1; i >= 0; i--) {
	assert(nodes->nodeTab[i]);
	
	xmlNodeSetContent(nodes->nodeTab[i], value);
	/*
	 * All the elements returned by an XPath query are pointers to
	 * elements from the tree *except* namespace nodes where the XPath
	 * semantic is different from the implementation in libxml2 tree.
	 * As a result when a returned node set is freed when
	 * xmlXPathFreeObject() is called, that routine must check the
	 * element type. But node from the returned set may have been removed
	 * by xmlNodeSetContent() resulting in access to freed data.
	 * This can be exercised by running
	 *       valgrind xpath2 test3.xml '//discarded' discarded
	 * There is 2 ways around it:
	 *   - make a copy of the pointers to the nodes from the result set 
	 *     then call xmlXPathFreeObject() and then modify the nodes
	 * or
	 *   - remove the reference to the modified nodes from the node set
	 *     as they are processed, if they are not namespace nodes.
	 */
	if (nodes->nodeTab[i]->type != XML_NAMESPACE_DECL)
	    nodes->nodeTab[i] = NULL;
    }
}

