### BakaProxy

BakaProxy is a tool to proxy the official *Bakaláři* Android app's connection to school server locally on your device, and along the way change class names (1.A, 2.A, A1) in timetables to more descriptive room code that might be present on your school. It can in theory be used for other substitutions, utilizing the XPath syntax, but it's currently hardcoded to only modify timetables for efficiency. The class_mapping.txt file included is ready for use on Gymso in year 2018/19, and might get updated each year if I'm not too lazy. The server address is currently hardcoded to bakalari.gymso.cz, but could be made configurable if there is demand. One person is enough demand in this case.

### Using

If you know your linux stuff, you can probably find you own way to utilize this. But if you don't, this step by step tutorial should help you:

1. download [Android Terminal Emulator](https://play.google.com/store/apps/details?id=jackpal.androidterm). If you hate Google with passion, as you should, you can [Get it on F-Droid](https://f-droid.org/en/packages/jackpal.androidterm/)

2. copy the precompiled/Bakaproxy folder to your Android devices's internal storage (where i.e your Download folder is) - there should now be /sdcard/BakaProxy/{the three files} ; note that most Android file browsers leave out the /sdcard/ part

3. open Android Terminal Emulator, and write in `cp -r /sdcard/BakaProxy $HOME` and hit enter; this should copy the folder to Android Terminal Emulator's internal data storage, which doesn't have program execution disabled; If you can't paste into the terminal, try using a different browser (i.e firefox) for copying the text; if hitting enter does nothing, your keyboard is broken and you will have to try another one - [Hacker's keyboard](https://github.com/klausw/hackerskeyboard) is amazingly intuitive to use once you get used to it, or you can unistall it later.

4. because /sdcard/ doesn't play well with file permissions, you will need to enter `chmod +x $HOME/BakaProxy/BakaProxy` and `chmod +x $HOME/BakaProxy/run.sh` as well

5. now all that's needed is creating a shortcut with AndroidTerminal to launch BakaProxy; first, enter `echo $HOME` - this will tell you where $HOME is; now, create a shortcut, and select Term Shortcut; now, in the "command" field, enter the output of `echo $HOME` + "/BakaProxy/run.sh" - i.e "/data/data/com.jackpal.androidterm/app_HOME/BakaProxy/run.sh" and don't forget to give the shortcut a name.

6. if all went well, you should have a shortcut for launching BakaProxy. Now, tell your Bakaláři app that the school's server is in fact 127.0.0.1:30000 (on your device), and you should see the modified timetable.

That's it! If you found this hard to follow, feel free to suggest improvements. Also if you know how to make this into a full-blown apk easily, using shortcuts to turn it on/off, definitely let me know. Preferably using C only, no java.

### Compiling

0. if you want to use bundled libs, skip 1. and 2.; otherwise, remove ./include and ./lib

1. edit GCC and gstreamer\_arch in download\_android\_dependencies.sh according to your device

2. run download\_android\_dependencies.sh

3. edit GCC in Makefile

4. run `make android`

5. the result will be ./BakaProxy binary
